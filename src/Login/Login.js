import React from "react";
import { useState } from "react";
import { Button, Checkbox, FormControlLabel, TextField, Link } from "@mui/material";

import "./Login.css"

//object Value
const innitFormValue = {
    Username: "",
    Password: "",
    // isPermission: false,
};
const data = [
    {
        Username: "admin",
        Password: "123",
    },
    {
        Username: "hung",
        Password: "a",
    },
];



export default function LoginFrame() {

    const [formValue, setFormValue] = useState(innitFormValue);
    const [errors, setErrors] = useState({});
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [showPassword, setShowPassword] = useState(false);


    const handleChange = (event) => {
        const { value, name } = event.target;
        setFormValue({
            ...formValue,
            [name]: value,
        });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (validateForm()) {
            console.log("form value", formValue);
            alert("Login success");
        } else {
            console.log("form errors", errors);
        }
    }

    const isEmptyValue = (value) => {
        return !value || value.trim().length < 1;
    }

    
    const validateForm = () => {
        const error = {};
        if (isEmptyValue(formValue.Username)) {
            error["Username"] = "Username is required";
        }
        if (isEmptyValue(formValue.Password)) {
            error["Password"] = "Password is required";
        }
        else //nếu mật khẩu không rỗng thì kiểm tra
        {
            const UserData = data.find((name)=> name.Username === formValue.Username)
            
            if(UserData){
                if(UserData.Password !== formValue.Password){
                    error["Password"] ="Wrong password";
                }
            }

        }
        setErrors(error);

        return Object.keys(error).length === 0;
    };

    const togglePassword= ()=>{
        setShowPassword(!showPassword);
    }


    return (
        <div className="Login-page">
            <div className="container">
                <h1 className="title">Login</h1>
                <form onSubmit={handleSubmit}>
                    <div className="f1">
                        <TextField id="Username"
                            className="input"
                            label="Username or Email"
                            variant="outlined"
                            name="Username"
                            value={formValue.Username}
                            onChange={handleChange}
                        >
                        </TextField>
                        {
                            errors.Username && (
                                <div className="errors-feedback">{errors.Username}</div>
                            )
                        }
                    </div>

                    <div className="f1">
                        <TextField id="Password"
                            className="input"
                            label="Password"
                            variant="outlined"
                            name="Password"
                            type={showPassword ? "text" : "password"}
                            value={formValue.Password}
                            onChange={handleChange}>
                        </TextField>
                        <Button onClick={togglePassword}>Show Password</Button>
                        {
                            errors.Password && (
                                <div className="errors-feedback">{errors.Password}</div>
                            )
                        }
                    </div>

                    <div className="f1">
                        <FormControlLabel   control={<Checkbox defaultChecked />}
                                            label="Remember me"
                                            className="input" />
                    </div>

                    <div className="f1">
                        <Link   href="#"
                                className="input"
                                variant="body2">
                                {'Forget Password'}
                        </Link>
                    </div>

                    <div className="f1">
                        <Button variant="contained"
                                type="Submit"
                        // onChange={handleSubmit}
                        >Login</Button>
                    </div>
                </form>
                {/* <div>
                <h3>Username:{formValue.Username}</h3>
                <h3>Password:{formValue.Password}</h3>
            </div> */}

            </div>
        </div>

    );
}